/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.hopper;

import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import edu.wpi.first.wpilibj.DigitalInput;

class HopperHal implements HopperHalI {
    private final VictorSPX m_motor = new VictorSPX(31);
    private final DigitalInput m_centerSensor = new DigitalInput(0);
    private final DigitalInput m_sideSensor = new DigitalInput(1);
    private final DigitalInput m_primeSensor = new DigitalInput(2);
    private static final double k_conveyorSpeed = 0.0; //TODO: find conveyor speed

    @Override 
    public void conveyorSpinIn() {
        m_motor.set(VictorSPXControlMode.PercentOutput, k_conveyorSpeed);
    }

    @Override 
    public void conveyorSpinOut() {
        m_motor.set(VictorSPXControlMode.PercentOutput, -k_conveyorSpeed);
    }

    @Override 
    public boolean centerSensor() {
        return m_centerSensor.get();
    }

    @Override 
    public boolean sideSensor() {
        return m_sideSensor.get();
    }

    @Override 
    public boolean primeSensor() {
        return m_primeSensor.get();
    }
}
