/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.hopper;
/* Conveyor-belt things: can use to move in or push out
belt connects to a wheel that helps push balls in
Sensors: the two squares on the plate*/

interface HopperHalI {
    void conveyorSpinIn();
    void conveyorSpinOut();
    boolean centerSensor();
    boolean sideSensor();
    boolean primeSensor();
}
