/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;

public class Intake {
    private IntakeHalI m_hal;
    
    public Command getRetractCommand() {
        return new InstantCommand(() -> {
            m_hal.retractArm();
        });
    }

    public Command getExtendCommand() {
        return new InstantCommand(() -> {
            m_hal.extendArm();
        });
    }

    public Command getSpinInCommand() {
        return new InstantCommand(() -> {
            m_hal.spinIn();
        });
    }
    
    public Command getSpinOutCommand() {
        return new InstantCommand(() -> {
            m_hal.spinOut();
        });
    }
}
