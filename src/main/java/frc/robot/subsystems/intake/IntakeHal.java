/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package frc.robot.subsystems.intake;

import com.ctre.phoenix.motorcontrol.VictorSPXControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;

public class IntakeHal implements IntakeHalI {
    private static final boolean k_extendValue = true;
    private static final double k_spinInSpeed = 0.2;
    private static final double k_spinOutSpeed = 0.2;

    private final Solenoid m_solenoid = new Solenoid(15, PneumaticsModuleType.CTREPCM, 0);
    private final VictorSPX m_victorSPX = new VictorSPX(30);

    @Override
    public void stopSpin() {
        m_victorSPX.set(VictorSPXControlMode.Disabled, 0.0);
    }

    @Override
    public void spinIn() {
        m_victorSPX.set(VictorSPXControlMode.PercentOutput, k_spinInSpeed);
    }

    @Override
    public void spinOut() {
        m_victorSPX.set(VictorSPXControlMode.PercentOutput, -k_spinOutSpeed);
    }

    @Override
    public void extendArm() {
        m_solenoid.set(k_extendValue);
    }
    
    @Override
    public void retractArm() {
        m_solenoid.set(!k_extendValue);
    }
}
